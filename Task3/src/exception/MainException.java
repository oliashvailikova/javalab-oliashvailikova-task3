package exception;

import pack.Package;

/**
 * Created by Admin on 21.01.17.
 */
public class MainException extends Exception {
    Package pack;
    public MainException(String message, Package pack){
        super(message);
        this.pack=pack;
    }



    public Package getPackageInfo(){
        return pack;
    }
    public String printException() {
        return getMessage()+"; Посылка:"+getPackageInfo().toString();
    }
}

