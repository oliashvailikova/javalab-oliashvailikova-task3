package exception;

import pack.Package;

/**
 * Created by Admin on 21.01.17.
 */
public class NotEnoughSpaceInTheStorage extends MainException {
    public NotEnoughSpaceInTheStorage(String message, Package pack) {
        super(message, pack);
    }
}


