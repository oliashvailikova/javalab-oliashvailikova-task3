package exception;


import pack.Package;

public class PlaceIsOcupiedException extends MainException {


    public PlaceIsOcupiedException(String message, Package pack) {
        super(message, pack);
    }
}

