package exception;


import pack.Package;

public class WrongSizeException extends MainException {

    public WrongSizeException(String message, Package pack) {
        super(message, pack);
    }
}