package pack;


import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.logging.Logger;

public class MainActivity {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(MainActivity.class.getName());
        ReceivingStation receivingStation = new ReceivingStation(logger);
        receivingStation.receive();
    }
}

