package pack;

public class Package {
    private String packageId;
    private int packageSize;
    private int placeId;

    Package(String packageId, int packageSize, int placeId) {
        this.packageId = packageId;
        this.packageSize = packageSize;
        this.placeId = placeId;
    }

    int getPackageSize() {
        return packageSize;
    }

    int getPlaceId() {
        return placeId;
    }

    public String toString() {
        return packageId + ";" + packageSize;
    }

    String getPackageId() {
        return packageId;
    }
}

