package pack;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import exception.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.*;

class ReceivingStation {
    private Logger logger;
    private static final String INPUT_FILE_NAME = "file.csv";
    private static final String LOG_FILE_NAME = "LogApp.txt";
    private static final String OUTPUT_FILE_NAME = "Storage.txt";
    private static final String STORAGE_AMOUNT_FILE_NAME = "numberOfWarehouses.txt";

    private int amountOfStorages;
    private String[] descriptionOfStorages;
    private List<Storage> storages = new ArrayList<>();

    private int currentStorageAtList = 0;

    private int deliveredPacks = 0;
    private int deliveredPacksSize = 0;
    private int notDeliveredPacks = 0;
    private int notDeliveredPacksSize = 0;

    ReceivingStation(Logger logger) {
        generateInputFile();
        this.logger = logger;
        try (BufferedReader reader = new BufferedReader(new FileReader(STORAGE_AMOUNT_FILE_NAME))) {
            String storagesInfo;
            if ((storagesInfo = reader.readLine()) != null) {
                //input file a;b;c;d;e;f... where a = amount of storages (<10),
                // b,c,d,e,f... = units amount for each storage
                descriptionOfStorages = storagesInfo.split(";");
                amountOfStorages = Integer.parseInt(descriptionOfStorages[0]);
                if (amountOfStorages > 10) {
                    throw new IOException("amount of storages > 10");
                } else if (amountOfStorages < descriptionOfStorages.length - 1) {
                    throw new IOException("amount of storages < amount of descriptions");
                }
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Invalid Data. Check input file." + ex.getMessage());
        }
    }

    private void generateInputFile() {
        int numberOfPacks = 10000;
        int numberOfRecipients = 100;
        int maxPackSize = 150;
        try (FileWriter writer = new FileWriter(INPUT_FILE_NAME)) {
            for (int i = 1; i <= numberOfPacks; i++) {
                writer.write("Package#" + i + ";"
                        + getRandomInt(1, numberOfRecipients) + ";"
                        + getRandomInt(1, maxPackSize)
                        + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getRandomInt(int min, int max) {
        Random random = new Random();

        return random.nextInt((max - min) + 1) + min;
    }

    void receive() {

        String line;
        for (int i = 1; i <= amountOfStorages; i++) {
            Multimap<Integer, Package> storageDescription = HashMultimap.create();
            int numberOfPlaces = Integer.parseInt(descriptionOfStorages[i]);
            storages.add(new Storage(i, storageDescription, numberOfPlaces));
        }

        try (BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE_NAME))) {
            FileHandler fh = new FileHandler(LOG_FILE_NAME);
            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
            while ((line = br.readLine()) != null) {
                String[] pos = line.split(";");
                Package parcel = new Package(pos[0], Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
                sendParcel(parcel);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Не удалось создать файл лога из-за ошибки ввода-вывода.", e);
        }
        addTotalInfo();
    }

    private void sendParcel(Package parcel) {

        try {
            storages.get(currentStorageAtList).sortingLine(parcel);
            deliveredPacks += 1;
            deliveredPacksSize += parcel.getPackageSize();
            packDelivered(parcel.getPackageId(), currentStorageAtList + 1, parcel.getPlaceId());
            currentStorageAtList = 0;
        } catch (MainException wse) {
            logger.log(Level.INFO, wse.printException());
            if (currentStorageAtList < amountOfStorages - 1) {
                currentStorageAtList++;
            } else {
                currentStorageAtList = 0;
                packNotDelivered(parcel);
                notDeliveredPacks += 1;
                notDeliveredPacksSize += parcel.getPackageSize();
                return;
            }
            sendParcel(parcel);
        }
    }

    private void addTotalInfo() {
        try (FileWriter writer = new FileWriter(OUTPUT_FILE_NAME, true)) {
            writer.write("Total delivered: " + deliveredPacks + ", " +
                    deliveredPacksSize + System.lineSeparator());
            writer.write("Total not delivered: " + notDeliveredPacks + ", " +
                    notDeliveredPacksSize + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void packNotDelivered(Package parcel) {
        try (FileWriter writer = new FileWriter(OUTPUT_FILE_NAME, true)) {
            writer.write("# " + parcel.getPackageId() + " => not delivered" + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void packDelivered(String packId, int storageNumber, int placeNumber) {
        try (FileWriter writer = new FileWriter(OUTPUT_FILE_NAME, true)) {
            writer.write("# " + packId + " => Storage #" + storageNumber + ", "
                    + placeNumber + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}