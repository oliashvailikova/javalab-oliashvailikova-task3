package pack;


import com.google.common.collect.Multimap;
import exception.EmptyFieldException;
import exception.NotEnoughSpaceInTheStorage;
import exception.PlaceIsOcupiedException;
import exception.WrongSizeException;

import java.util.HashMap;
import java.util.Map;


class Storage {

    private Multimap<Integer, Package> storage;
    private Map<Integer, Integer> freeSpace; //<placeId, freeSpace>

    Storage(int storageNumber, Multimap<Integer, Package> storage, int numberOfPlaces) {
        this.storage = storage;
        this.freeSpace = new HashMap<>();
        for (int i = 1; i <= numberOfPlaces; i++) {
            freeSpace.put(i, calculatePlaceSize(storageNumber, i));
        }
    }

    private int calculatePlaceSize(int storageNumber, int placeNumber) {
        return ((storageNumber * (placeNumber)) % 10) * 10 + (storageNumber * (placeNumber)) / 10;
    }

    void sortingLine(Package parcel)
            throws WrongSizeException, EmptyFieldException, PlaceIsOcupiedException, NotEnoughSpaceInTheStorage {
        if (parcel.getPlaceId() == 0 || parcel.getPackageSize() == 0) {
            throw new EmptyFieldException("Отсутствует необходимая информация", parcel);
        } else {
            putThePackage(parcel);
        }
    }

    private void putThePackage(Package parcel) throws PlaceIsOcupiedException, NotEnoughSpaceInTheStorage,
            WrongSizeException {
        int placeId = parcel.getPlaceId();
        if (storage.containsKey(placeId)) {
            int space = freeSpace.get(placeId) - parcel.getPackageSize();
            if (placeId > freeSpace.get(placeId)) {
                throw new WrongSizeException("Размер посылки превышает размер ячейки", parcel);
            } else if (space >= 0) {
                freeSpace.put(placeId, space);
                storage.put(placeId, parcel);
            } else throw new PlaceIsOcupiedException("Место на складе занято", parcel);
        } else if (freeSpace.size() >= parcel.getPlaceId()) {
            storage.put(parcel.getPlaceId(), parcel);
        } else {
            throw new NotEnoughSpaceInTheStorage("На складе нет такой ячейки", parcel);
        }
    }
}